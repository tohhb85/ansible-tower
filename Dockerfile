FROM centos
MAINTAINER Toh Haw Beng (tohhb85@gmail.com)
RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
RUN yum install -y epel-release
RUN yum install -y ansible python38 git-core
RUN easy_install-3.8 pip
RUN pip install --upgrade pip setuptools
RUN pip install ansible
RUN pip install boto
RUN pip install boto3
RUN ansible-galaxy collection install amazon.aws
RUN ansible-galaxy install geerlingguy.apache geerlingguy.mysql geerlingguy.php geerlingguy.java geerlingguy.tomcat6 geerlingguy.solr
RUN yum install -y unzip
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-2.0.30.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip
RUN ./aws/install
RUN aws configure set aws_access_key_id ASIA3N5CO56GOBLWXYUA
RUN aws configure set aws_secret_access_key euB26utZClwIVKpDEd5eZOgFWTBY7UsiLfnfhDIJ
RUN aws configure set aws_session_token FwoGZXIvYXdzEG0aDMKOspqPViEsS2PpUCK9AaQxT1JMGnPKLuj3xcZC+T511cfMLrWaZfHgt0PhxzOhUQ7H0IKuRakmo8UPbAh6K2l5Eso4Cg1GgkSm3AaIT0ol6IhlDosmqN+GGfrSpl8Z015P2s/Ovv6WWfs6i6yGNb4FyTTj7cXvKTVbzzxJC2ZgVTLOiVxaU1zRPNZzfFlVtgTxCjJDT1xpGdApnkWkXjMlQu7px3JDOHfRSBMNDNwL8v5GCa8u2/0Pb7IVDWJboybnnGPX9oljXV5+Aiiz9fGIBjItgpRa6EXBBtDVPiwJixIg8f+zYDsbT1hVu0oF1PhmPyzIdys0USYI7a1njCSG
RUN aws configure set region us-east-1
RUN aws configure set output yaml
RUN aws ec2 create-key-pair --key-name my-key-pair --query 'KeyMaterial' --output text > my-key-pair.pem
RUN chmod 400 my-key-pair.pem
CMD ["/bin/bash"]
